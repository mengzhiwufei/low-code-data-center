package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.EmpSubPartyWin;
import com.yabushan.system.service.IEmpSubPartyWinService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 员工奖惩子集Controller
 *
 * @author yabushan
 * @date 2021-03-21
 */
@RestController
@RequestMapping("/system/win")
public class EmpSubPartyWinController extends BaseController
{
    @Autowired
    private IEmpSubPartyWinService empSubPartyWinService;

    /**
     * 查询员工奖惩子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:win:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmpSubPartyWin empSubPartyWin)
    {
        startPage();
        List<EmpSubPartyWin> list = empSubPartyWinService.selectEmpSubPartyWinList(empSubPartyWin);
        return getDataTable(list);
    }

    /**
     * 导出员工奖惩子集列表
     */
    @PreAuthorize("@ss.hasPermi('system:win:export')")
    @Log(title = "员工奖惩子集", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmpSubPartyWin empSubPartyWin)
    {
        List<EmpSubPartyWin> list = empSubPartyWinService.selectEmpSubPartyWinList(empSubPartyWin);
        ExcelUtil<EmpSubPartyWin> util = new ExcelUtil<EmpSubPartyWin>(EmpSubPartyWin.class);
        return util.exportExcel(list, "win");
    }

    /**
     * 获取员工奖惩子集详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:win:query')")
    @GetMapping(value = "/{recId}")
    public AjaxResult getInfo(@PathVariable("recId") String recId)
    {
        return AjaxResult.success(empSubPartyWinService.selectEmpSubPartyWinById(recId));
    }

    /**
     * 新增员工奖惩子集
     */
    @PreAuthorize("@ss.hasPermi('system:win:add')")
    @Log(title = "员工奖惩子集", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmpSubPartyWin empSubPartyWin)
    {
        return toAjax(empSubPartyWinService.insertEmpSubPartyWin(empSubPartyWin));
    }

    /**
     * 修改员工奖惩子集
     */
    @PreAuthorize("@ss.hasPermi('system:win:edit')")
    @Log(title = "员工奖惩子集", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmpSubPartyWin empSubPartyWin)
    {
        return toAjax(empSubPartyWinService.updateEmpSubPartyWin(empSubPartyWin));
    }

    /**
     * 删除员工奖惩子集
     */
    @PreAuthorize("@ss.hasPermi('system:win:remove')")
    @Log(title = "员工奖惩子集", businessType = BusinessType.DELETE)
	@DeleteMapping("/{recIds}")
    public AjaxResult remove(@PathVariable String[] recIds)
    {
        return toAjax(empSubPartyWinService.deleteEmpSubPartyWinByIds(recIds));
    }
}
