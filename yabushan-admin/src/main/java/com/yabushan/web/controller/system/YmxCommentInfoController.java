package com.yabushan.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.system.domain.YmxCommentInfo;
import com.yabushan.system.service.IYmxCommentInfoService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 评论Controller
 *
 * @author yabushan
 * @date 2021-04-02
 */
@RestController
@RequestMapping("/ymx/ymxcommentinfo")
public class YmxCommentInfoController extends BaseController
{
    @Autowired
    private IYmxCommentInfoService ymxCommentInfoService;

    /**
     * 查询评论列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcommentinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(YmxCommentInfo ymxCommentInfo)
    {
        startPage();
        List<YmxCommentInfo> list = ymxCommentInfoService.selectYmxCommentInfoList(ymxCommentInfo);
        return getDataTable(list);
    }

    /**
     * 导出评论列表
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcommentinfo:export')")
    @Log(title = "评论", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(YmxCommentInfo ymxCommentInfo)
    {
        List<YmxCommentInfo> list = ymxCommentInfoService.selectYmxCommentInfoList(ymxCommentInfo);
        ExcelUtil<YmxCommentInfo> util = new ExcelUtil<YmxCommentInfo>(YmxCommentInfo.class);
        return util.exportExcel(list, "ymxcommentinfo");
    }

    /**
     * 获取评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcommentinfo:query')")
    @GetMapping(value = "/{commentId}")
    public AjaxResult getInfo(@PathVariable("commentId") String commentId)
    {
        return AjaxResult.success(ymxCommentInfoService.selectYmxCommentInfoById(commentId));
    }

    /**
     * 新增评论
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcommentinfo:add')")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody YmxCommentInfo ymxCommentInfo)
    {
        return toAjax(ymxCommentInfoService.insertYmxCommentInfo(ymxCommentInfo));
    }

    /**
     * 修改评论
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcommentinfo:edit')")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody YmxCommentInfo ymxCommentInfo)
    {
        return toAjax(ymxCommentInfoService.updateYmxCommentInfo(ymxCommentInfo));
    }

    /**
     * 删除评论
     */
    @PreAuthorize("@ss.hasPermi('ymx:ymxcommentinfo:remove')")
    @Log(title = "评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{commentIds}")
    public AjaxResult remove(@PathVariable String[] commentIds)
    {
        return toAjax(ymxCommentInfoService.deleteYmxCommentInfoByIds(commentIds));
    }
}
